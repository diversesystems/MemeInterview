import java.util.*;

public class Main{
    public static void main(String[] args)
    {
        odd100();
        System.out.println();
        sumtotalbetweentwoint(0,100);
        System.out.println();
        countdownfrom(700,200,13);
        System.out.println();
        drawtworandom();
        System.out.println();
        findalloddsgiven(0,100);
        System.out.println();
        drawtworandomexpand();
        System.out.println();
    }

    //find odds between 0 and 100
    public static void odd100(){
        for(int i = 0; i <= 100; i++)
        {
            if(i % 2 != 0)
            {
                System.out.print(i + " ");
            }
        }
    }

    //find sum total between two numbers inclusive
    public static void sumtotalbetweentwoint(int x, int y)
    {
        int sum = 0;
        for(int i = x; i <= y; i++)
        {
            sum += i;
        } 

        System.out.print(sum);
    }

    //count down from one number to a bottom limit decremented by a pattern
    public static void countdownfrom(int start, int end, int decrement){
        for(int i = start; i >= end; i-=decrement)
        {
            System.out.print(i + " ");
        }
    }

    //really basic draw two cards from a standard deck of cards (52)
    //could be expanded on
    public static void drawtworandom()
    {
        boolean cont = true;
        Random rand = new Random();
        int c1 = rand.nextInt(53);
        int c2 = -1;

        while(cont)
        {
            int r = rand.nextInt(53);
            if(c1 != r)
            {
                c2 = r;
                cont = false;
            }
        }
    }

    //find all odds given two limits
    public static void findalloddsgiven(int x, int y)
    {
        int i;

        if(x%2 == 0)
            i = x+1;
        else
            i = x;

        for(; i <= y; i+=2){
            System.out.print(i + " ");
        }
    }

    //make a standard deck of cards
    //shuffle them
    //draw two random
    public static void drawtworandomexpand()
    {
        String[] number = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}; //1 = ace
        String[] face = {"J", "Q", "K"};
        String[] suits = {"D", "S", "C", "H"};
        ArrayList<String> deck = new ArrayList<>();

        for(int i = 0 ; i < suits.length; i++)
        {
            for(int j = 0; j < number.length; j++)
            {
                deck.add(number[j]+suits[i]);
            }
            for(int k = 0; k < face.length; k++)
            {
                deck.add(face[k]+suits[i]);
            }
        }

        Collections.shuffle(deck);

        Random rand = new Random();
        String c1 = deck.get(rand.nextInt(deck.size()));
        System.out.println(c1);
        String c2 = "";
        boolean cont = true;

        while(cont)
        {
            c2 = deck.get(rand.nextInt(deck.size()));
            if(!c2.equals(c1))
                cont = false;
            else
                c2 = "";
        }
        System.out.println(c2);

    }

    public static void fizzbuzz()
    {
        // most general implementation
        for(int i = 1; i <= 100; i++)
        {
            if(i % 3 == 0 && i % 5 == 0)
            {
                System.out.print("FizzBuzz ");
            }
            else if(i % 3 == 0)
            {
                System.out.print("Fizz ");
            }
            else if(i % 5 == 0)
            {
                System.out.print("Buzz ");
            }
            else{
                System.out.print(i + " ");
            }
        }

        //interesting implementation
        for (int i = 1; i < = 100; i++)
        {
            String outp = "";
            if (i % 3 == 0) outp = "Fizz";
            if (i % 5 == 0) outp += "Buzz";
            if (outp == "") outp = Integer.toString(i);
            System.out.print(outp + " ");
        }

        //shortest, using ternary operators within ternary operators
        for(int i=1; i < 101; i++) {
            System.out.println(i%15==0?"FizzBuzz ":i%3==0?"Fizz ":i%5==0?"Buzz ":i);
        }

    }

}